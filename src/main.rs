use std::env;
use std::error::Error as StdError;
use std::fmt::{Debug, Display, Formatter, Result as FmtResult};

use lazy_static::lazy_static;

enum Error {
	Usage(String),
	IllegalChar(char),
	IllegalVerb(String)
}

impl Debug for Error {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f, "{}", self)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		let msg = match self {
			Self::Usage(name) => format!("usage: {} tech/html/name [{}]+", name, LEGALCH.as_str()),
			Self::IllegalChar(ch) => format!("illegal sequence char: {}!", ch),
			Self::IllegalVerb(verb) => format!("illegal verb: {}!", verb),
		};

		write!(f, "{}", msg)
	}
}

impl StdError for Error {
}

const SEQTAB: [(char, &str); 9] = [
	('L', "m_@.# + c_@."),
	('Q', "a_@.#^2. + b_@.# + c_@."),
	('C', "a_@.#^3. + b_@.#^2. + c_@.# + d_@."),
	('U', "a_@.#^4. + b_@.#^3. + c_@.#^2. + d_@.# + e_@."),
	('I', "a_@.#^5. + b_@.#^4. + c_@.#^3. + d_@.#^2. + e_@.# + f_@."),
	('G', "h_@.r_@.^# - 1."),
	('S', "a_@.sin b_@.(# + c_@.) + d_@."),
	('O', "a_@.cos b_@.(# + c_@.) + D_@."),
	('H', "1/(a_@.# + b_2.)"),
];

const NAMETAB: [(char, &str, &str, &str, &str); 9] = [
	('L', "linear", "line", "ne", "near"),
	('Q', "quadratic", "quadri", "qua", "tic"),
	('C', "cubic", "cubi", "bi", "bic"),
	('U', "quartic", "quarti", "rti", "rtic"),
	('I', "quintic", "quinti", "nti", "ntic"),
	('G', "geometric", "geo", "me", "tric"),
	('S', "sinusoidal", "sinu", "sovi", "soidic"),
	('O', "cosinusoidal", "cosinu", "covi", "coidic"),
	('H', "harmonic", "harmo", "moni", "monic"),
];

lazy_static! {
	static ref LEGALCH: String = {
		let mut s = String::new();
		for seqent in SEQTAB {
			s.push(seqent.0);
		}
		s
	};
}

fn genseq(seqdef: &str) -> Result<String, Error> {
	let mut seq = String::from("#");
	let mut seqn = 1;

	for ch in seqdef.chars().rev() {
		if !LEGALCH.contains(ch) {
			return Err(Error::IllegalChar(ch));
		}

		for seqent in SEQTAB {
			if seqent.0 != ch {
				continue;
			}

			let sub = if seqn > 1 {
				format!("({})", seqent.1)
			} else {
				seqent.1.to_string()
			};

			seq = seq.replace("#", &sub);
			seq = seq.replace("@", &seqn.to_string());
		}

		seqn += 1;
	}

	Ok(format!("t_n. = {}", seq).replace("#", "n"))
}

fn seqtohtml(seq: &str) -> String {
	let mut seq_html = String::new();
	let mut attrstk = Vec::new();

	for ch in seq.chars() {
		match ch {
			'_' => {
				seq_html.push_str("<sub>");
				attrstk.push("sub");
			}
			'^' => {
				seq_html.push_str("<sup>");
				attrstk.push("sup");
			}
			'.' => {
				let tag = attrstk.pop().unwrap();
				seq_html.push_str(&format!("</{}>", tag));
			}
			_ => seq_html.push(ch),
		}
	}

	seq_html
}

fn genname(seqdef: &str) -> Result<String, Error> {
	for ch in seqdef.chars() {
		if !LEGALCH.contains(ch) {
			return Err(Error::IllegalChar(ch));
		}
	}

	if seqdef.len() == 1 {
		for ent in NAMETAB {
			if ent.0 == seqdef.chars().nth(0).unwrap() {
				return Ok(ent.1.to_string());
			}
		}
	}

	let nameseg = |i, ch| {
		for nameent in NAMETAB {
			if nameent.0 != ch {
				continue;
			}

			return if i == 0 {
				nameent.2
			} else if i < seqdef.len() - 1 {
				nameent.3
			} else {
				nameent.4
			};
		}
		""
	};

	let mut name = String::new();
	for (i, ch) in seqdef.chars().enumerate() {
		name += nameseg(i, ch);
	}

	Ok(name)
}

fn main() -> Result<(), Error> {
	let args = env::args().collect::<Vec<_>>();
	if args.len() != 3 {
		return Err(Error::Usage(args[0].clone()));
	}

	match args[1].as_str() {
		"tech" => println!("{}", genseq(&args[2])?),
		"html" => println!("{}", seqtohtml(&genseq(&args[2])?)),
		"name" => println!("{}", genname(&args[2])?),
		_ => return Err(Error::IllegalVerb(args[1].clone())),
	}

	Ok(())
}
